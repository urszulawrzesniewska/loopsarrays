import java.util.Scanner;

public class zadanie3poprawione {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Data size:");
        int size = scanner.nextInt();
        
        int[] data = new int[size];
        
        System.out.println("Data:");
        for (int i = 0; i < data.length; i++) {
            data[i] = scanner.nextInt();
        }
        
        int sum = 0;
        int product = 1;
        for (int i = 0; i < data.length; i++) {
            sum += data[i];
            product *= data[i];
        }
        System.out.println("Sum: " + sum);
        System.out.println("Product: " + product);
    }   
    
}
